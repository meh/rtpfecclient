#include <gst/gst.h>
#include <stdlib.h>

#define MAKE_AND_ADD(var, pipe, name, label)                                   \
  G_STMT_START                                                                 \
  {                                                                            \
    GError* make_and_add_err = NULL;                                           \
    if (G_UNLIKELY(!(var = (gst_parse_bin_from_description_full(               \
                       name,                                                   \
                       TRUE,                                                   \
                       NULL,                                                   \
                       GST_PARSE_FLAG_NO_SINGLE_ELEMENT_BINS,                  \
                       &make_and_add_err))))) {                                \
      GST_ERROR(                                                               \
        "Could not create element %s (%s)", name, make_and_add_err->message);  \
      g_clear_error(&make_and_add_err);                                        \
      goto label;                                                              \
    }                                                                          \
    if (G_UNLIKELY(!gst_bin_add(GST_BIN_CAST(pipe), var))) {                   \
      GST_ERROR("Could not add element %s", name);                             \
      goto label;                                                              \
    }                                                                          \
  }                                                                            \
  G_STMT_END

typedef struct
{
  GstElement *video_depay;
  GstElement *audio_depay;
  gdouble audio_drop_probability;
  gdouble video_drop_probability;
} State;

static GstElement *
plug_video (GstElement *pipeline, GstElement *rtpbin, State *state)
{
  GstElement *ret = NULL;
  GstElement *rtpsrc, *rtcpsrc, *netsim, *depay, *queue, *dec, *conv, *scale, *filter, *sink;
  GstCaps *caps;

  MAKE_AND_ADD (rtpsrc, pipeline, "udpsrc", done);
  MAKE_AND_ADD (rtcpsrc, pipeline, "udpsrc", done);
  MAKE_AND_ADD (netsim, pipeline, "netsim", done);
  MAKE_AND_ADD (depay, pipeline, "rtpvp8depay", done);
  MAKE_AND_ADD (queue, pipeline, "queue", done);
  MAKE_AND_ADD (dec, pipeline, "vp8dec", done);
  MAKE_AND_ADD (conv, pipeline, "videoconvert", done);
  MAKE_AND_ADD (scale, pipeline, "videoscale", done);
  MAKE_AND_ADD (filter, pipeline, "capsfilter", done);
  MAKE_AND_ADD (sink, pipeline, "autovideosink", done);

  if (!gst_element_link_many (depay, queue, dec, conv, scale, filter, sink, NULL)) {
    GST_ERROR ("Failed to link video elements");
    goto done;
  }

  if (!gst_element_link (rtpsrc, netsim)) {
    GST_ERROR ("Failed to link rtp video source");
    goto done;
  }

  if (!gst_element_link_pads (netsim, "src", rtpbin, "recv_rtp_sink_0")) {
    GST_ERROR ("Failed to link rtpbin RTP video pad");
    goto done;
  }

  if (!gst_element_link_pads (rtcpsrc, "src", rtpbin, "recv_rtcp_sink_0")) {
    GST_ERROR ("Failed to link rtpbin RTCP video pad");
    goto done;
  }

  caps = gst_caps_new_simple ("application/x-rtp", "clock-rate", G_TYPE_INT, 90000, NULL);
  g_object_set (rtpsrc, "address", "127.0.0.1", "port", 5000, "caps", caps, NULL);
  gst_caps_unref (caps);
  g_object_set (rtcpsrc, "address", "127.0.0.1", "port", 5001, NULL);
  g_object_set (netsim, "drop-probability", state->video_drop_probability, NULL);
  caps = gst_caps_new_simple ("video/x-raw", "width", G_TYPE_INT, 1920, "height", G_TYPE_INT, 1080, NULL);
  g_object_set (filter, "caps", caps, NULL);
  gst_caps_unref (caps);

  ret = depay;

done:
  return ret;
}

static GstElement *
plug_audio (GstElement *pipeline, GstElement *rtpbin, State *state)
{
  GstElement *ret = NULL;
  GstElement *rtpsrc, *rtcpsrc, *netsim, *depay, *queue, *dec, *conv, *resample, *sink;
  GstCaps *caps;

  MAKE_AND_ADD (rtpsrc, pipeline, "udpsrc", done);
  MAKE_AND_ADD (rtcpsrc, pipeline, "udpsrc", done);
  MAKE_AND_ADD (netsim, pipeline, "netsim", done);
  MAKE_AND_ADD (depay, pipeline, "rtpopusdepay", done);
  MAKE_AND_ADD (queue, pipeline, "queue", done);
  MAKE_AND_ADD (dec, pipeline, "opusdec", done);
  MAKE_AND_ADD (conv, pipeline, "audioconvert", done);
  MAKE_AND_ADD (resample, pipeline, "audioresample", done);
  MAKE_AND_ADD (sink, pipeline, "autoaudiosink", done);

  if (!gst_element_link_many (depay, queue, dec, conv, resample, sink, NULL)) {
    GST_ERROR ("Failed to link audio elements");
    goto done;
  }

  if (!gst_element_link (rtpsrc, netsim)) {
    GST_ERROR ("Failed to link rtp audio source");
    goto done;
  }

  if (!gst_element_link_pads (netsim, "src", rtpbin, "recv_rtp_sink_1")) {
    GST_ERROR ("Failed to link rtpbin RTP audio pad");
    goto done;
  }

  if (!gst_element_link_pads (rtcpsrc, "src", rtpbin, "recv_rtcp_sink_1")) {
    GST_ERROR ("Failed to link rtpbin RTCP audio pad");
    goto done;
  }

  caps = gst_caps_new_simple ("application/x-rtp", "clock-rate", G_TYPE_INT, 48000, NULL);
  g_object_set (rtpsrc, "address", "127.0.0.1", "port", 5002, "caps", caps, NULL);
  gst_caps_unref (caps);
  g_object_set (rtcpsrc, "address", "127.0.0.1", "port", 5003, NULL);
  g_object_set (netsim, "drop-probability", state->audio_drop_probability, NULL);
  g_object_set (dec, "plc", TRUE, "use-inband-fec", TRUE, NULL);

  ret = depay;

done:
  return ret;
}

static void
new_storage_cb (GstElement *rtpbin, GstElement *storage)
{
  g_object_set (storage, "size-time", 2500000000, NULL);
}

static GstCaps *
request_pt_map_cb (GstElement * rtpbin, guint session_id, guint pt)
{
  switch (pt) {
    case 100:
      return gst_caps_new_simple ("application/x-rtp",
          "media", G_TYPE_STRING, "video",
          "clock-rate", G_TYPE_INT, 90000,
          "is-fec", G_TYPE_BOOLEAN, TRUE,
          NULL);
    case 96:
      return gst_caps_new_simple ("application/x-rtp",
          "media", G_TYPE_STRING, "video",
          "clock-rate", G_TYPE_INT, 90000,
          "encoding-name", G_TYPE_STRING, "VP8",
          NULL);
    case 97:
      return gst_caps_new_simple ("application/x-rtp",
          "media", G_TYPE_STRING, "audio",
          "clock-rate", G_TYPE_INT, 48000,
          "encoding-name", G_TYPE_STRING, "OPUS",
          NULL);
    default:
      return NULL;
  }
}

static GstElement *
request_fec_decoder_cb (GstElement *rtpbin, guint sess_id)
{
  GstElement *ret = NULL;

  if (sess_id == 0) {
    ret = gst_element_factory_make ("rtpulpfecdec", NULL);
    GObject *internal_storage;

    g_signal_emit_by_name (rtpbin, "get-internal-storage", sess_id,
        &internal_storage);

    g_object_set (ret, "storage", internal_storage, "pt", 100, NULL);

    g_object_unref (internal_storage);
  }

  return ret;
}

static void
pad_added_cb (GstElement *element, GstPad *pad, State *state)
{
  GstCaps *caps = gst_pad_get_current_caps (pad);
  GstStructure *s = gst_caps_get_structure (caps, 0);
  const gchar *mtype;
  GstPad *peerpad = NULL;

  mtype = gst_structure_get_string (s, "media");

  if (!g_strcmp0 (mtype, "video"))
    peerpad = gst_element_get_static_pad (state->video_depay, "sink");
  else if (!g_strcmp0 (mtype, "audio"))
    peerpad = gst_element_get_static_pad (state->audio_depay, "sink");

  if (peerpad) {
    gst_pad_link (pad, peerpad);
    gst_object_unref (peerpad);
  }

  gst_caps_unref (caps);
}

static GstElement *
build_pipeline(State *state)
{
  GstElement *pipeline = gst_pipeline_new (NULL);
  GstElement *rtpbin;

  MAKE_AND_ADD (rtpbin, pipeline, "rtpbin", err);

  g_signal_connect (rtpbin, "new-storage", G_CALLBACK (new_storage_cb), NULL);

  if (!(state->video_depay = plug_video (pipeline, rtpbin, state)))
    goto err;

  if (!(state->audio_depay = plug_audio (pipeline, rtpbin, state)))
    goto err;

  g_signal_connect (rtpbin, "request-pt-map", G_CALLBACK (request_pt_map_cb), NULL);
  g_signal_connect (rtpbin, "request-fec-decoder", G_CALLBACK (request_fec_decoder_cb), NULL);
  g_signal_connect (rtpbin, "pad-added", G_CALLBACK (pad_added_cb), state);
  g_object_set (rtpbin, "do-lost", TRUE, NULL);

done:
  return pipeline;

err:
  gst_object_unref (pipeline);
  pipeline = NULL;
  goto done;
}

int main(int ac, char **av)
{
  GstElement *pipeline;
  GstBus *bus;
  GstMessage *msg;
  gst_init (NULL, NULL);
  gboolean done = FALSE;
  State state;
  gchar *vendptr, *aendptr;

  if (ac != 3) {
    gst_printerr ("Usage: %s VIDEO_DROP_PROBABILITY AUDIO_DROP_PROBABILITY\n", av[0]);
    return 1;
  }

  state.video_drop_probability = strtod (av[1], &vendptr);
  state.audio_drop_probability = strtod (av[2], &aendptr);

  if (*vendptr != '\0' || state.video_drop_probability < 0 ||
      state.video_drop_probability > 1.0f) {
    gst_printerr ("VIDEO_DROP_PROBABILITY must be a float between 0.0 and 1.0\n");
    return 1;
  }

  if (*aendptr != '\0' || state.audio_drop_probability < 0 ||
      state.audio_drop_probability > 1.0f) {
    gst_printerr ("AUDIO_DROP_PROBABILITY must be a float between 0.0 and 1.0\n");
    return 1;
  }

  if (!(pipeline = build_pipeline (&state)))
    return 1;

  bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline));
  gst_element_set_state (pipeline, GST_STATE_PLAYING);

  while (!done) {
    msg = gst_bus_timed_pop (bus, GST_CLOCK_TIME_NONE);

    switch GST_MESSAGE_TYPE (msg) {
      case GST_MESSAGE_EOS:
        done = TRUE;
        gst_printerr ("End of Stream\n");
        break;
      case GST_MESSAGE_ERROR: {
        GError *err = NULL;
        gchar *dbg;
        gst_message_parse_error (msg, &err, &dbg);
        gst_printerr ("Error: %s (%s)\n", err->message, dbg);
        g_error_free (err);
        g_free (dbg);
        done = TRUE;
        break;
      }
      case GST_MESSAGE_LATENCY:
        gst_printerr ("Recalculating latency\n");
        gst_bin_recalculate_latency (GST_BIN (pipeline));
        break;
      case GST_MESSAGE_STATE_CHANGED:
        if (GST_MESSAGE_SRC (msg) == GST_OBJECT (pipeline)) {
          GstState state;

          gst_message_parse_state_changed (msg, &state, NULL, NULL);
          if (state == GST_STATE_PLAYING)
            GST_DEBUG_BIN_TO_DOT_FILE (GST_BIN (pipeline), GST_DEBUG_GRAPH_SHOW_ALL, "client-playing");
        }
        break;
      default:
        break;
    }
    gst_message_unref (msg);
  }

  gst_object_unref (bus);
  gst_element_set_state (pipeline, GST_STATE_NULL);
  gst_object_unref (pipeline);

  gst_printerr ("Bye!");

  return 0;
}
